/* SPDX-License-Identifier: CC-BY-NC-ND-4.0 */
                .section .rodata
                .global ototo_kun
                .type   ototo_kun, @object
                .align  4
ototo_kun:       .incbin "noid.mdesk"
ototo_kun_end:   .global ototo_kun_size
                .type   ototo_kun_size, @object
                .align  4
ototo_kun_size:  .int    ototo_kun_end - ototo_kun
