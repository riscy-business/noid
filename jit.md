### State Diagram

```mermaid
stateDiagram-v2
    [*]-->HexOne
    HexOne-->HexTwo : Valid Hex
    HexOne-->[*] : Syntax Error
    HexTwo-->HexOne : Found Hex
    HexTwo-->StartDot : Found Start Dot
    HexTwo-->OpenCurly : Found Open Curly
    HexTwo-->[*] : EOS
    HexTwo-->[*] : Syntax Error
    StartDot-->Minus : Found Minus
    StartDot-->OffOne : Found Hex
    StartDot-->[*] : Syntax Error
    OffOne-->OffTwo : Valid Hex
    OffOne-->[*] : Syntax Error
    OffTwo-->EndDot : Found End Dot
    OffTwo-->[*] : Syntax Error
    Minus-->OffOne : Found Hex
    Minus-->[*] : Syntax Error
    EndDot-->HexOne : Found Hex
    EndDot-->[*] : EOS
    EndDot-->[*] : Syntax Error
    OpenCurly-->NonCloser : Found Char
    OpenCurly-->[*] : Syntax Error
    NonCloser-->NonCloser : Valid Char
    NonCloser-->CloseCurly : Found Close Curly
    NonCloser-->[*] : Syntax Error
    CloseCurly-->HexOne : Found Hex
    CloseCurly-->[*] : EOS
    CloseCurly-->[*] : Syntax Error
    CloseCurly-->[*] : Symbol Not Found
```

### Class & State Tables

| Class       | # | Range       |
|-------------|---|-------------|
| Hex         | 0 | 0-9,a-f,A-F |
| Error       | 1 | 00-1F,7F-FF |
| Dot         | 2 | .           |
| Open Curly  | 3 | {           |
| Close Curly | 4 | }           |
| Minus       | 5 | -           |
| Other       | 6 | None of ^   |

| State      | # |
|------------|---|
| Failure    | 0 | 00 00 00 00
| HexOne     | 1 | 00 00 00 01
| HexTwo     | 2 | 00 00 00 10
| OpenDot    | 3 | 00 00 00 11
| Minus      | 4 | 00 00 01 00
| OffOne     | 5 | 00 00 01 01
| OffTwo     | 6 | 10 00 01 10
| CloseDot   | 7 | 00 00 01 11
| OpenCurly  | 8 | 01 01 10 00
| NonCloser  | 9 | 00 00 10 01
| CloseCurly | A | 00 10 10 10

### Byte-To-Class

                  LOW NIBBLE
        0 1 2 3 4 5 6 7 8 9 A B C D E F
      0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
      1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
    H 2 6 6 6 6 6 5 6 6 6 6 6 6 6 6 2 6
    I 3 0 0 0 0 0 0 0 0 0 0 6 6 6 6 6 6
    G 4 6 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
    H 5 0 0 0 0 0 0 0 0 0 0 0 6 6 6 6 6
      6 6 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
    N 7 0 0 0 0 0 0 0 0 0 0 0 3 6 4 6 1
    I 8 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
    B 9 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
    B A 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
    L B 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
    E C 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
      D 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
      E 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
      F 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1

### What Next
The next thing to do would be to examine the usage code and come up with better 
mappings. From there, a pre-multiplied transition table can be drawn up and a 
final table-driven implementation written.

### Conclusion

After sketching up what the usage code would look like, it became apparent that 
table-driving would not make the JIT simpler. I did not see an obvious way for the 
tables to help drive the actual work. It only helps the parsing, but this simply 
becomes a classic exercise of translating a regex to a FSM. The whole point of 
machine-code strings is to make parsing as trivial as possible, so naturally tables 
are not going to reduce complexity here. I'd rather have straight code to maintain 
than tables in this case. Moving forward, I will change the syntax for offsets and symbols:

* Offsets -- .XXXXXXXX where X is a hex digit. (Two's-complement 32-bit)
* Symbols -- :XXXXXXXXsymbol (Two's complement 32-bit length-prefix)

These changes will simplify parsing without table-driving it & as these strings 
are not human-readable to begin with, it is fine that this is less human-friendly.

In any case, it was a good exercise to sketch up the FSM as it will aid in 
ensuring we handle all cases correctly.

### Updated State Diagram

```mermaid
stateDiagram-v2
    [*]-->HexIter
    HexIter-->HexLast : Iteration 1
    HexIter-->[*] : Syntax Error
    HexLast-->HexIter : Next Pair
    HexLast-->Dot : Found Dot
    HexLast-->Colon : Found Colon
    HexLast-->[*] : EOS
    HexLast-->[*] : Syntax Error
    Dot-->OffIter : Found Hex
    Dot-->[*] : Syntax Error
    OffIter-->OffIter : Iterations 1 through 7
    OffIter-->OffLast : Iteration 8
    OffIter-->[*] : Syntax Error
    OffLast-->HexIter : Found Hex
    OffLast-->Colon : Found Colon
    OffLast-->Dot : Found Dot
    OffLast-->[*] : EOS
    OffLast-->[*] : Syntax Error
    Colon-->LenIter : Found Hex
    Colon-->[*] : Syntax Error
    LenIter-->LenIter : Iterations 1 through 7
    LenIter-->LenLast : Iteration 8
    LenLast-->CharIter : Found Char
    LenLast-->[*] : Syntax Error
    CharIter-->CharIter : Iterations 1 through Len minus 1
    CharIter-->CharLast : Iteration Len
    CharIter-->[*] : Syntax Error
    CharLast-->HexIter : Found Hex
    CharLast-->Colon : Found Colon
    CharLast-->Dot : Found Dot
    CharLast-->[*] : EOS
    CharLast-->[*] : Symbol Not Found
    CharLast-->[*] : Syntax Error
```
