#if 0
# SPDX-License-Identifier: CC-BY-NC-ND-4.0
set -euxo pipefail # [^1]
IFS=$'\n\t' # [^1]
DIR=$(mktemp -d)
function finish { # [^2]
    rm -r $DIR
}
trap finish EXIT # [^2]
gcc -c ototo_kun.s -o $DIR/blob.o
gcc -I3rdparty/stb -I3rdparty/metadesk/source nee_san.c $DIR/blob.o -rdynamic -ldl -o $DIR/bin
$DIR/bin
# perf stat -r 100 -- $DIR/bin
exit
# @SECTION Bibliography
# [^1]: Maxwell, A. (n.d.). Use Bash Strict Mode (Unless You Love Debugging) [web log]. Retrieved October 12, 2021, from http://redsymbol.net/articles/unofficial-bash-strict-mode/.
# [^2]: Maxwell, A. (n.d.). How "Exit Traps" Can Make Your Bash Scripts Way More Robust And Reliable [web log]. Retrieved October 12, 2021, from http://redsymbol.net/articles/bash-exit-traps/.
#endif
/* SPDX-License-Identifier: CC-BY-NC-ND-4.0 */
//@SECTION headers
#include <stddef.h> //size_t
#include <stdint.h> //[u]int{8|16|32|64}_t
#include <stdio.h> //fprintf, stderr
#include <stdlib.h> //EXIT_FAILURE, EXIT_SUCCESS, free, malloc, posix_memalign
#include <string.h> //MAP_ANONYMOUS, MAP_PRIVATE, PROT_EXEC, PROT_READ, PROT_WRITE, memcpy, memset
#include <dlfcn.h> //RTLD_LAZY, RTLD_NOLOAD, dlopen, dlsym
#include <sys/mman.h> //mmap, mprotect
#include <sys/user.h> //PAGE_SIZE
#define STB_DS_IMPLEMENTATION
#include "stb_ds.h"
#include "md.h"
#include "md.c"
//@SECTION sugar
#define BIT(n) (1ULL << n)
#define PAD__(pre, sym, post) pre##sym##post
#define PAD_(pre, sym, post) PAD__(pre, sym, post)
#define PAD(bytes) u8 PAD_(pad, __COUNTER__, _) [bytes]
#define cast
#define decl
#define global
#define maybe_inline static inline
#define out
#define persistent static
#define proc __attribute__((__noinline__))
//@SECTION constants
#define JIT_SPACE_SZ (PAGE_SIZE)
#define POOL_SZ (1ULL << 20)
#define OBJ_PER_POOL (POOL_SZ / sizeof (N_object))
enum { FREE = BIT(63), HIDDEN = BIT(62) };
//@SECTION macros
#define object_flags(n) (cast (u64)((n)->p_)&0xFFFF000000000000ULL)
#define dentry_lbl(n) (cast (u8 *)(cast (u64)((n)->p_)&0x0000FFFFFFFFFFFFULL))
//@SECTION type declarations
decl typedef struct N_dentry N_dentry;
decl typedef struct N_inode N_inode;
decl typedef struct N_iterator N_iterator;
decl typedef struct N_jiterr N_jiterr;
decl typedef struct N_object N_object;
//@SECTION typedefs
typedef int8_t s8;
typedef int32_t s32;
typedef int64_t s64;
typedef uint8_t u8;
typedef uint32_t u32;
typedef uint64_t u64;
typedef size_t uX; //[^1, gistfile1.txt#167]
typedef void N_traversal_callback(N_dentry *);
//@SECTION procedure declarations
decl void define_primitive(N_dentry *);
decl u8 * str_intern(u8 *);
decl void tag_put(N_dentry *, u8 *, N_dentry **);
decl void traverse_depth_postorder(N_dentry *, N_iterator *, N_traversal_callback *, u64 flags);
decl u32 type_intern(u8 *);
decl u8 * type_string(u32);
//@SECTION structs
struct N_dentry
{
    N_dentry * parent;
    N_dentry ** children;
    N_inode * node;
    u8 * p_; //@NOTE: N_object flags packed in upper 16-bits
             //     : N_dentry label packed in lower 48-bits
};
struct N_inode
{
    void * value;
    PAD(8); //@NOTE: reserved for packing up to 128-bit inline in `value`
    u64 size;
    u32 type;
    u32 link_count;
};
struct N_iterator
{
    N_dentry ** front;
    u64 length;
};
struct N_jiterr
{
    u8 * msg;
    u64 col;
}; //@NOTE: packs into inode as 128-bit inline `value`
struct N_object
{
    N_dentry dentry;
    N_inode inode;
}; //@NOTE: 64-byte cache-line alignment
//@SECTION globals
global void * dl_main_;
//@NOTE: value used for type ids, otherwise 0
global struct { u8 * key; u32 value; } * interns_;
global u8 * jit_space_;
global MD_Arena * md_space_;
global u64 object_cnt_;
//@NOTE: could keep free-list sorted from high-address to low-address (better contiguity)
global N_object ** objects_free_;
global N_object * objects_last_;
global N_object ** pools_;
global struct { u8 * key; void (* value)(N_dentry *); } * primitives_;
//@SECTION externs
extern u8 ototo_kun [];
extern s32 ototo_kun_size;
//@SECTION code
//@SECTION private layer
maybe_inline void transform_node(N_dentry * node)
{
    if (node->node == NULL) return;
    if (node->node->type == type_intern("Primitive"))
        shget(primitives_, node->node->value)(node);
    else if (node->node->type == type_intern("Built-in"))
        define_primitive(node);
}
//@SECTION private platform layer
maybe_inline void PLT_init()
{
    dl_main_ = dlopen(NULL, RTLD_LAZY|RTLD_NOLOAD);
    jit_space_ = mmap(NULL, JIT_SPACE_SZ,
            PROT_READ|PROT_EXEC,
            MAP_PRIVATE|MAP_ANONYMOUS, -1, 0);
    md_space_ = MD_ArenaAlloc(1 << 29); //oink oink
    sh_new_strdup(primitives_);
    shdefault(interns_, 0);
    sh_new_arena(interns_);
    str_intern("Unknown"); //@NOTE: used as a place-holder before Ototo-kun bootstraps a richer type-system
                           //     : str_intern used here for type id 0
    type_intern("Built-in");
    type_intern("Primitive");
    type_intern("JIT Error");
}
maybe_inline s32 PLT_jit_begin()
{
    return mprotect(jit_space_, JIT_SPACE_SZ, PROT_READ|PROT_WRITE);
}
maybe_inline s32 PLT_jit_end()
{
    return mprotect(jit_space_, JIT_SPACE_SZ, PROT_READ|PROT_EXEC);
}
//@SECTION public platform layer
proc void * PLT_jit(u8 * code, out u64 * col)
{
    persistent s8 hex_lut [] = {
        /*     0  1  2  3  4  5  6  7  8  9  A  B  C  D  E  F */
        /*0*/ -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
        /*1*/ -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
        /*2*/ -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,46,-1,
        /*3*/  0, 1, 2, 3, 4, 5, 6, 7, 8, 9,58,-1,-1,-1,-1,-1,
        /*4*/ -1,10,11,12,13,14,15,-1,-1,-1,-1,-1,-1,-1,-1,-1,
        /*5*/ -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
        /*6*/ -1,10,11,12,13,14,15,-1,-1,-1,-1,-1,-1,-1,-1,-1,
        /*7*/ -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
        /*8*/ -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
        /*9*/ -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
        /*A*/ -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
        /*B*/ -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
        /*C*/ -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
        /*D*/ -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
        /*E*/ -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
        /*F*/ -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
    };
    persistent s32 free_off;
    u8 * sym = NULL;
    u8 * jit_space = cast (u8 *)jit_space_ + free_off;
    void * result = jit_space;
    u8 * jitted = jit_space;
    u8 * cursor = code;
    s64 val = 0;
    u32 run = 2, n;
    PLT_jit_begin();
    for (u32 state = 0; *cursor; ++cursor)
    {
        s8 c = hex_lut[*cursor];
        if (c == -1) goto SYNTAX_ERROR;
        if (!run)
        {
            state = c;
            if (c <= 15){ run = 2; goto HEX; }
            //@NOTE: 32-bit => 8 hex digits
            run = 8; continue;
        }
        if (c > 15) goto SYNTAX_ERROR;
HEX:    val <<= 4; val |= c;
        if (--run) continue;
        switch (state)
        {
            default:  n = 1; break;
            //@NOTE: While offset/len are 32-bit, the result is 64-bit.
            case '.': n = 8; val = cast (s64)jitted + cast (s32)val; break;
            //@NOTE: ++cursor because we are on the char BEFORE the string
            case ':': n = 8; sym = ++cursor; cursor += val; goto RESOLVE;
        }
JIT:    memcpy(jitted, &val, n); jitted += n; val = 0;
        continue;
    }
    if (run) goto SYNTAX_ERROR;
    free_off += jitted - jit_space;
    PLT_jit_end();
    return result;
RESOLVE:
    {u8 * e = sym; while (*e++); if (e < cursor) goto SYNTAX_ERROR;}
    u8 c = *cursor; *cursor = '\0';
    val = cast (s64)dlsym(dl_main_, sym);
    *cursor = c; --cursor; //@NOTE: --cursor to cancel the coming loop increment
    if (val != cast (s64)NULL) goto JIT;
    goto SYMBOL_NOT_FOUND;
ROLLBACK:
    memset(jit_space, 0, jitted - jit_space);
    PLT_jit_end();
    return result;
SYMBOL_NOT_FOUND:
    *col = cast (u64)(cursor - code);
    result = cast (void*)-404LL;
    goto ROLLBACK;
SYNTAX_ERROR:
    *col = cast (u64)(cursor - code);
    result = cast (void*)-400LL;
    goto ROLLBACK;
}
proc void PLT_object_put(N_object * new)
{
    if (object_cnt_*sizeof (N_object) % POOL_SZ == 0) goto RECYCLE;
    memcpy(++objects_last_, new, sizeof (N_object));
    ++object_cnt_;
    return;
RECYCLE:
    if (!arrlen(objects_free_)) goto NEW_POOL;
    N_object * thrift = arrpop(objects_free_);
    memcpy(thrift, new, sizeof (N_object));
    objects_last_ = thrift;
    return;
NEW_POOL:
    N_object * new_pool = NULL;
    posix_memalign(cast (void **)&new_pool, POOL_SZ, POOL_SZ);
    arrput(pools_, new_pool);
    objects_last_ = new_pool;
    memcpy(objects_last_, new, sizeof (N_object));
    ++object_cnt_;
    return;
}
//@SECTION: public layer
proc void define_primitive(N_dentry * node)
{
    N_dentry * def = arrpop(node->children);
    N_dentry * lbl = arrpop(node->children);
    arrfree(node->children);
    free(node->node->value);
    N_jiterr e = {0};
    void * result = PLT_jit(def->node->value, &e.col);
    switch (cast (s64)result)
    {
        case -400:
            e.msg = str_intern("Syntax Error");
            goto ERROR;
        case -404:
            e.msg = str_intern("Symbol Not Found");
            goto ERROR;
        default:
            shput(primitives_, lbl->node->value, result);
            node->node->value = def->node->value;
            node->node->size = def->node->size;
            //@NOTE: Ototo-kun is responsible for defining primitives,
            //     : so he must know what this is. Also we tag it as System
            node->node->type = type_intern("Unknown");
            break;
    }
    def->p_ = cast (u8 *)(cast (u64)def->p_ | FREE);
    arrput(objects_free_, cast (N_object *)def);
FINISH_UP:
    node->p_ = lbl->node->value;
    tag_put(node, "System", NULL);
    lbl->p_ = cast (u8 *)(cast (u64)lbl->p_ | FREE);
    arrput(objects_free_, cast (N_object *)lbl);
    return;
ERROR:
    memcpy(&node->node->value, &e, sizeof (N_jiterr));
    node->node->size = 0;
    node->node->type = type_intern("JIT Error");
    N_dentry ** args = NULL; arrput(args, def);
    tag_put(node, "Source", args);
    fprintf(stderr, "%s:%d:\n%s\n", e.msg, e.col, def->node->value);
    for (uX i = e.col; i; --i) fprintf(stderr, " ");
    fprintf(stderr, "^\n");
    goto FINISH_UP;
}
proc void iterator_next(N_iterator * i)
{
    --(i->length);
    ++(i->front);
}
proc void stack_put(N_iterator ** stack, N_dentry ** arr)
{
    arrput(*stack, ((N_iterator){arr, arrlen(arr)}));
}
//@NOTE: if we want non-null terminated strings interned later,
//     : we'll want to roll our own str_intern_range like ion instead of using stb_ds
proc u8 * str_intern(u8 * str) // [^2, ion/common.c#L347]
{
    u8 * key = shgetp(interns_, str)->key;
    if (key) return key;
    shput(interns_, str, 0);
    key = shgetp(interns_, str)->key;
    return key;
}
proc void tag_put(N_dentry * node, u8 * label, N_dentry ** args)
{
    N_dentry * tags = NULL;
    for (uX i = 0; i < arrlen(node->children); ++i)
    {
        if (object_flags(node->children[i])&HIDDEN == 0 || dentry_lbl(node->children[i]) == NULL)
            continue;
        if (dentry_lbl(node->children[i]) != str_intern("tags"))
            continue;
        tags = node->children[i];
        break;
    }
    if (tags == NULL)
    {
        N_object new = {0};
        new.dentry.p_ = str_intern("tags");
        new.dentry.p_ = cast (u8 *)(cast (u64)new.dentry.p_|HIDDEN);
        PLT_object_put(&new);
        arrput(node->children, &objects_last_->dentry);
        objects_last_->dentry.parent = node;
        tags = &objects_last_->dentry;
    }
    N_object new = {0};
    new.dentry.p_ = str_intern(label);
    new.dentry.children = args;
    PLT_object_put(&new);
    arrput(tags->children, &objects_last_->dentry);
    objects_last_->dentry.parent = tags;
}
//@NOTE(Nee-san): Ototo-kun, don't be a tree-fucker! #FAFO
proc void transform(N_dentry * root)
{
    N_iterator * stack = NULL;
    traverse_depth_postorder(root, stack, transform_node, 0);
    arrfree(stack);
}
proc void traverse_depth_postorder(N_dentry * root, N_iterator * stack, N_traversal_callback * cbk, u64 flags)
{
    u64 traverse_hidden = flags&BIT(0);
    //@NOTE: This is an iterative postorder DFS which is equivalent to RPN
    s32 CHILDREN_RESOLVED = 0;
    if (root == NULL) root = &pools_[0]->dentry;
    if (arrlen(root->children) > 0)
        stack_put(&stack, root->children);
    while (arrlen(stack) > 0)
    {
        N_dentry ** unwrapped = arrlast(stack).front;
        if (CHILDREN_RESOLVED) goto HANDLE_PARENT;
        {
            //@NOTE: it is important that we traverse in this order for code execution
            N_dentry * node = unwrapped[0];
            if (!traverse_hidden && object_flags(node)&HIDDEN)
            {
                iterator_next(&arrlast(stack));
                continue;
            }
            if (arrlen(node->children) > 0)
            {
                stack_put(&stack, node->children);
                continue;
            }
            iterator_next(&arrlast(stack));
            cbk(node);
            CHILDREN_RESOLVED = arrlast(stack).length == 0
                ? arrpop(stack), 1
                : 0;
        }
        continue;
HANDLE_PARENT:
        {
            N_dentry * node = unwrapped[0];
            iterator_next(&arrlast(stack));
            cbk(node);
            CHILDREN_RESOLVED = arrlast(stack).length == 0
                ? arrpop(stack), 1
                : 0;
        }
        continue;
    }
    cbk(root);
}
proc u32 type_intern(u8 * str)
{
    persistent u32 counter = 1;
    u32 value = shget(interns_, str);
    if (value) return value;
    value = counter++;
    shput(interns_, str, value);
    return value;
}
proc u8 * type_string(u32 id)
{
    for (uX i = 0; i < shlen(interns_); ++i)
        if (interns_[i].value) return interns_[i].key;
    return NULL;
}
//@SECTION: main
int main(int argc, char * argv [])
{
    PLT_init();
    {
        N_object root = {0};
        root.dentry.p_ = str_intern("/");
        PLT_object_put(&root);
        tag_put(&objects_last_->dentry, "System", NULL);
    }
    {
        //@NOTE: We expect a well-formed document (Ototo-kun is embedded)
        MD_Node * root = MD_ParseWholeString(md_space_, MD_S8Lit(""),
                MD_S8(ototo_kun, ototo_kun_size)).node;
        MD_Node * match = MD_ChildFromString(root, MD_S8Lit("Inodes"), 0);
        for (MD_EachNode(node, match->first_child))
        {
            N_object parsed = {0};
            if (MD_NodeHasTag(node, MD_S8Lit("Type"), 0))
            {
                MD_Node * type = MD_TagArgFromIndex(node, MD_S8Lit("Type"), 0, 0);
                //@NOTE: Wouldn't need to copy if we used a str_intern_range
                u8 type_str [16] = {0};
                memcpy(type_str, type->string.str, type->string.size);
                parsed.inode.type = type_intern(type_str);
            }
            {
                u64 str_sz = node->string.size;
                parsed.inode.size = str_sz + 1;
                parsed.inode.value = malloc(parsed.inode.size);
                memcpy(parsed.inode.value, node->string.str, str_sz);
                (cast (u8 *)parsed.inode.value)[str_sz] = '\0';
            }
            PLT_object_put(&parsed);
        }
        match = MD_ChildFromString(root, MD_S8Lit("Dentries"), 0);
        uX i = 3;
        for (MD_EachNode(node, match->first_child), ++i)
        {
            N_object * inode_obj = NULL;
            if (i == object_cnt_)
                PLT_object_put(&(N_object){0});
            N_object * dentry_obj = pools_[i/OBJ_PER_POOL] + i%OBJ_PER_POOL;
            {
                //@NOTE: + 3 to account for `/.tags/System`
                u64 inode_idx = MD_U64FromString(node->string, 10) + 3;
                u64 inode_pool = inode_idx/OBJ_PER_POOL;
                inode_idx %= OBJ_PER_POOL;
                inode_obj = pools_[inode_pool]+inode_idx;
                dentry_obj->dentry.node = &inode_obj->inode;
            }
            ++(inode_obj->inode.link_count);
            if (!MD_NodeHasTag(node, MD_S8Lit("Parent"), 0))
            {
                arrput(pools_[0]->dentry.children, &dentry_obj->dentry);
                dentry_obj->dentry.parent = &pools_[0]->dentry;
            }
            else
            {
                //@NOTE: + 3 to account for `/.tags/System`
                u64 parent_idx = MD_U64FromString(MD_TagArgFromIndex(node,
                            MD_S8Lit("Parent"), 0, 0)->string, 10) + 3;
                u64 parent_pool = parent_idx/OBJ_PER_POOL;
                parent_idx %= OBJ_PER_POOL;
                N_object * parent = pools_[parent_pool]+parent_idx;
                arrput(parent->dentry.children, &dentry_obj->dentry);
                dentry_obj->dentry.parent = &parent->dentry;
            }
        }
    }
    MD_ArenaClear(md_space_);
    transform(NULL); //the rest is up to you, Ototo-kun!
    return EXIT_SUCCESS;
}
/* @SECTION Bibliography
 * [^1]: Giesen, F. (2016, May 8). A bit of background on compilers exploiting signed overflow [web log]. GitHub Gist. Retrieved November 14, 2021, from https://gist.github.com/rygorous/e0f055bfb74e3d5f0af20690759de5a7.
 * [^2]: Vognsen, P. (2018, April 9). Version (day13 @ 7844393). Bitwise. Handmade Network. Retrieved October 12, 2021, from https://github.com/pervognsen/bitwise.git */
